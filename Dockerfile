FROM quay.io/ansible/default-test-container:5.6.2

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN apt-get update -y \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
  docker.io \
  && apt-get clean \
  && rm -r /var/lib/apt/lists/*

RUN pip install -U pip wheel tox \
  && rm -rf ~/.cache/pip
